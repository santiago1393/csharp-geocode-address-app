﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Text;
using System.Globalization;
using System.Net.Http;
using System.Web;
using System.Diagnostics;

namespace GoogleMapsGeocodeApp
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
            string key = Properties.Settings.Default["API_KEY"].ToString();
            if (!string.IsNullOrEmpty(key))
            {
                this.txtApiKey.Text = key;
                txtAddress.Focus();
            }
        }

        private string api_key = "";
        private string maps_url = "https://maps.googleapis.com/maps/api/geocode/";
        private string address = "";
        private string[] addresses;
        private string file = "";
        private string folder = "";
        private string country = "";

        public ResultGeocode geoCodeAddress(string add)
        {
            ResultGeocode resultGeocode = new ResultGeocode();
            resultGeocode.location = new Location();
            resultGeocode.location.lat = 0;
            resultGeocode.location.lng = 0;
            resultGeocode.message = string.Empty;
            resultGeocode.address = add;
            resultGeocode.old_address = add;
            try
            {
                resultGeocode.address = resultGeocode.address.Replace(' ', '+');
                string encodAdd = HttpUtility.UrlEncode(resultGeocode.address);
                //string request = maps_url + "json?" + "address=" + resultGeocode.address + "&region=" + this.country + "&key=" + this.api_key;
                string request = maps_url + "json?" + "address=" + encodAdd + "&region=" + this.country + "&key=" + this.api_key;
                string requestEncod = HttpUtility.UrlEncode(request);
                WebClient wc = new WebClient();
                wc.Encoding = Encoding.UTF8;
                var json = wc.DownloadString(request);
                JavaScriptSerializer ser = new JavaScriptSerializer();
                
                RootObject root = ser.Deserialize<RootObject>(json);
                Result result = new Result();
                if(root.results.Count != 0)
                {
                    result = root.results[0];
                    resultGeocode.location.lng = result.geometry.location.lng;
                    resultGeocode.location.lat = result.geometry.location.lat;
                    resultGeocode.address = result.formatted_address;
                }
                else
                {
                    resultGeocode.location.lat = 0;
                    resultGeocode.location.lng = 0;
                    resultGeocode.address = "";
                    resultGeocode.message = "Results not found, try another address";
                }
              
                switch (root.status)
                {
                    case "OK":                       
                        resultGeocode.message = "Result OK";                        
                        break;
                    case "ZERO_RESULTS":
                        resultGeocode.message = "Results not found, try another address";
                        break;
                    case "OVER_DAILY_LIMIT":
                        resultGeocode.message = "API / ACOUNT Error, Check the , verify the permissions and payment options";
                        break;
                    case "OVER_QUERY_LIMIT":
                        resultGeocode.message = "Quota Exceeded!";
                        break;
                    case "REQUEST_DENIED":
                        resultGeocode.message = "The request was denied";
                        break;
                    case "INVALID_REQUEST":
                        resultGeocode.message = "Invalid request, bad address format";
                        break;
                    case "UNKNOWN_ERROR":
                        resultGeocode.message = "Error on the server, try again";
                        break;
                    default:
                        resultGeocode.message = "Invalidad Error: " + root.status ;
                        break;
                }
                
            }
            catch (Exception ex)
            {
                resultGeocode.message = ex.Message;              
            }
            return resultGeocode;
        }
                

      

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
                if (result == DialogResult.OK) // Test result.
                {
                    this.file = openFileDialog1.FileName;
                    lblFileName.Text = lblFileName.Text + " " + file;
                    this.addresses = File.ReadAllLines(file, Encoding.UTF8);                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + Environment.NewLine + ex.Message + Environment.NewLine + "Try Again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.addresses = new string[0];
                this.file = string.Empty;
            }           
        }

        private void btnBulkGeocode_Click(object sender, EventArgs e)
        {
            try
            {

                this.api_key = this.txtApiKey.Text;
                if (this.file == string.Empty || this.api_key == string.Empty || this.folder == string.Empty)
                {
                    if (this.api_key == string.Empty)
                    {
                        MessageBox.Show("Error, you must enter the Google Maps API_KEY", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.txtApiKey.Focus();
                        return;
                    }
                    else
                    {
                        if(this.file == string.Empty)
                        {
                            MessageBox.Show("Error, you must select a file with Directions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.btnSelectFile.Focus();
                            return;
                        }
                        else
                        {
                            MessageBox.Show("Error, you must select a location to store results", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.btnSelectFolder.Focus();
                            return;
                        }
                    }
                }
                else
                {
                    folder += "\\result.csv";
                    using (File.Create(folder))
                    {

                    }
                    TextWriter tw = new StreamWriter(folder, false, Encoding.UTF8);
                    tw.WriteLine("Input_Address|Result_Address|Resultado|Coord_lon|Coord_lat");
                    foreach (string item in this.addresses)
                    {
                        if (string.Empty != item)
                        {
                            ResultGeocode res = this.geoCodeAddress(item);
                            tw.WriteLine(res.old_address + "|" + res.address + "|" + res.location.lng + "|" + res.location.lat + "|" + res.message);
                        }
                        else
                        {
                            tw.WriteLine("Missing Address");
                        }
                    }
                    tw.Close();
                    MessageBox.Show("Process completed successfully, review the results in the file: " + this.folder, "Process Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + Environment.NewLine + ex.Message + Environment.NewLine + "Try Again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.file = string.Empty;
                this.folder = string.Empty;
            }
        }
                

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = folderBrowserDialog1.ShowDialog();
                if (res == DialogResult.OK)
                {
                    this.folder = folderBrowserDialog1.SelectedPath;
                    lblResultFolder.Text = lblResultFolder.Text + " " + this.folder;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + Environment.NewLine + ex.Message + Environment.NewLine + "Try Again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.folder = string.Empty;
                this.file = string.Empty;
            }
        }

        private void btnSingleGeocode_Click(object sender, EventArgs e)
        {
            try
            {
                this.api_key = this.txtApiKey.Text;
                this.address = this.txtAddress.Text;
                if (this.api_key == string.Empty || this.address == string.Empty)
                {
                    if (this.api_key == string.Empty)
                    {
                        MessageBox.Show("Warning, please enter the Google Maps API_KEY", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.txtApiKey.Focus();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Warning, please enter a validad address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.txtAddress.Focus();
                        return;
                    }

                }
                else
                {
                    ResultGeocode res = this.geoCodeAddress(this.address);
                    // this.lblResult.Text = "Direccion ingresada: " + res.old_address + Environment.NewLine + "Dirección Estandarizada: " + res.address + Environment.NewLine + "Resultado: " + res.message + Environment.NewLine + "Coord_lon: " + res.location.lng + Environment.NewLine + "Coord_lat: " + res.location.lat;
                    this.txtLat.Text = res.location.lat.ToString();
                    this.txtLong.Text = res.location.lng.ToString();
                    this.txtResult.Text  = res.message.ToString();
                    if (res.message.Contains("OK"))
                    {
                        lblResult.BackColor = System.Drawing.Color.SpringGreen;
                        btnMap.Enabled = true;
                    }
                    else
                    {
                        lblResult.BackColor = System.Drawing.Color.LightCoral;
                    }
                    this.txtResult.TextAlign = HorizontalAlignment.Center;
                 
                }
            }
            catch (Exception ex)
            {
                lblResult.Text = "An unexpected error occurred: " + Environment.NewLine + ex.Message;
            }
        }

        private void Inicio_Load(object sender, EventArgs e)
        {
            lstCountry.Enabled = false;
            try
            {
                string requestCountries = "https://restcountries.eu/rest/v2/all?fields=languages;name";
                var json = new WebClient().DownloadString(requestCountries);
                JavaScriptSerializer ser = new JavaScriptSerializer();
                List<Pais> paises = ser.Deserialize<List<Pais>>(json);
                foreach (var pais in paises)
                {
                    lstCountry.Items.Add(new ComboboxItem(pais.name, pais.languages[0].iso639_1));
                }
            }
            catch (Exception)
            {
                lstCountry.Items.Clear();
                lstCountry.Enabled = false;
                chkCountry.Enabled = false;
            }
           
        }

        private void chkCountry_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCountry.Checked)
            {
                lstCountry.Enabled = true;
            }
            else
            {
                lstCountry.Enabled = false;
            }
        }

        private void lstCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            string val = ((ComboboxItem)lstCountry.Items[lstCountry.SelectedIndex]).Value.ToString();
            this.country = val;
        }

        private void chkSaveKey_CheckedChanged(object sender, EventArgs e)
        {
            // Borrar KEY
            if(this.chkSaveKey.Checked == false)
            {
                Properties.Settings.Default["API_KEY"] = "";
                Properties.Settings.Default.Save();
            }
        }

        private void txtApiKey_Leave(object sender, EventArgs e)
        {
            if( this.txtApiKey.Text.Length > 0)
            {
                Properties.Settings.Default["API_KEY"] = this.txtApiKey.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void btnMap_Click(object sender, EventArgs e)
        {
            string lat = this.txtLat.Text.Replace(",", ".");
            string lon = this.txtLong.Text.Replace(",", ".");
            string url = "https://maps.google.com/?q=" + lat + "," + lon ;          
            Process.Start(url);
        }
    }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public ComboboxItem(string txt, object val)
        {
            this.Text = txt;
            this.Value = val;
        }

        public override string ToString()
        {
            return Text;
        }
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }

    public class RootObject
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }

    public class ResultGeocode
    {
        public string address { set; get; }
        public string old_address { set; get; }
        public Location location { set; get; }
        public string message { set; get; }
    }

    public class Language
    {
        public string iso639_1 { get; set; }
        public string iso639_2 { get; set; }
        public string name { get; set; }
        public string nativeName { get; set; }
    }

    public class Pais
    {
        public List<Language> languages { get; set; }
        public string name { get; set; }
    }
}
